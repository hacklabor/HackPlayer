#include <Arduino.h>
#include <PubSubClient.h>
#include <DFRobotDFPlayerMini.h>

#if defined(ESP8266)
  #include <ESP8266WiFi.h>
  #include <WiFiClient.h>
  #include <ESP8266WebServer.h>
#elif defined(ESP32)
  #include <WiFi.h>
  #include <WiFiClient.h>
  #include <WebServer.h>
#endif

#include <ElegantOTA.h>
#include <secrets.h>


#if defined(ESP8266)
  ESP8266WebServer server(80);
#elif defined(ESP32)
  WebServer server(80);
#endif


#include <SoftwareSerial.h>
    SoftwareSerial mp3(D3, D4); // RX, TX
DFRobotDFPlayerMini myDFPlayer;
// MP3 Player



//Wifi
WiFiClient espClient;
PubSubClient client(espClient);


void callback(char* topic, byte* payload, unsigned int length) {
  String command;
  int track;
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (unsigned int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    command = command + String((char)payload[i]);
  }
  Serial.println();
  Serial.println(command);
  track = command.toInt();
  myDFPlayer.playMp3Folder(track);

  
  Serial.print("Play ");
  Serial.write(track);
  Serial.println(".mp3 in MP3 folder");
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(willClientID,mqtt_user,mqtt_password, willTopic, willQoS, willRetain, willOffMessage)) {
      Serial.println("connected");

      // Once connected, publish an announcement...
      client.publish(willTopic, willOnMessage, willRetain);
      // ... and resubscribe
      client.subscribe(mainTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void Wifi_ElegantOTA()
{
  WiFi.mode(WIFI_STA);
  WiFi.setHostname(STAHOST);
  WiFi.begin(STASSID, STAPSK);
  WiFi.setHostname(STAHOST);

  while (WiFi.waitForConnectResult() != WL_CONNECTED)
  {
    // Serial.println("Connection Failed! Rebooting...");
    delay(5000);
  }

    server.on("/", []() {
    server.send(200, "text/plain", "Hi! I am ESP8266 \n OTA Update unter http://[hostname]/update ");
  });


  ElegantOTA.begin(&server); // Start ElegantOTA
  server.begin();
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup() {
  Serial.begin(115200);
  mp3.begin(9600);
  Wifi_ElegantOTA();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  if (!myDFPlayer.begin(mp3)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    //while(true);
  }
  Serial.println(F("DFPlayer Mini online."));

  myDFPlayer.volume(30);  //Set volume value. From 0 to 30
  
  
  
 
}

void loop() {
  
}