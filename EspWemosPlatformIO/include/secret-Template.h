#include <Arduino.h>
const char* STASSID="Hacklabor";
const char* STAPSK="Password";
const char* STAHOST="esp-template";
// MQTT Server
const char* mqtt_server = "MQTT Broker";
const char* mqtt_user = "UserName";
const char* mqtt_password = "Password";

// MQTT Subscribes
const char* mainTopic = "Topic/#";


// MQTT Last will and Testament
byte willQoS = 0;

const char* willTopic = "lwt/esp-topic";
const char* willOnMessage = "online";
const char* willOffMessage = "offline";
const char* willClientID = "esp-ClientID";
boolean willRetain = true;